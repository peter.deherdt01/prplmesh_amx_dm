/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>

#include "prplmesh.h"

static prplmesh_app_t app;

static void print_info(amxc_var_t* config) {

    printf("config = \n");
    fflush(stdout);
    amxc_var_dump(config, STDOUT_FILENO);
}

amxd_dm_t* prplmesh_get_dm(void) {
    return &app.dm;
}

amxo_parser_t* prplmesh_get_parser(void) {
    return &app.parser;
}

amxb_bus_ctx_t* prplmesh_get_bus_ctx(void) {
    return app.bus_ctx;
}

int prplmesh_load_odl(amxd_dm_t* dm,
                      amxo_parser_t* parser,
                      const char* odl_file) {
    int retval = 0;
    amxd_object_t* root_obj = amxd_dm_get_root(dm);

    /* when loading ODL files at start-up it is a good idea to disable the
     * eventing system, otherwise when the eventloop is started, it could be
     *  flooded with events that you probably don't handle
     */

    // disable eventing while loading odls
    amxp_sigmngr_enable(&dm->sigmngr, false);
    retval = amxo_parser_parse_file(parser, odl_file, root_obj);
    // everything loaded, enable eventing
    amxp_sigmngr_enable(&dm->sigmngr, true);

    // parsing done dump config (for debugging)
    if(retval == 0) {
        print_info(&parser->config);
    }

    return retval;
}

int prplmesh_init(const char* uri, const char* odl) {
    int retval = 0;
    app.bus_ctx = NULL;
    amxo_parser_init(&app.parser);
    amxd_dm_init(&app.dm);

    /*
     * This initialization sequence is not fixed.
     * You can first load one or more odls and then create a bus connection
     *
     * It is however important that you load the correct bus back-ends (adaptor)
     * before trying to set-up a connection.
     *
     * To make the data model public it needs to be registered, this can only
     * be done when you have a connection available.
     * The data model is only accessible from the busses (connections) it is
     * registered on.
     */

    // A bus specific backend is needed before a connection can be created
    if(prplmesh_load_backend() != 0) {
        retval = 1;
        goto exit;
    }
    // Create a connection to a bus, a URI is needed.
    if(prplmesh_connect(uri, &app.bus_ctx) != 0) {
        retval = 2;
        goto exit;
    }
    // Load the data model from an odl file
    if(prplmesh_load_odl(&app.dm, &app.parser, odl) != 0) {
        retval = 3;
        goto exit;
    }
    // Register the data model
    if(prplmesh_register(app.bus_ctx, &app.dm) != 0) {
        retval = 4;
        goto exit;
    }

exit:
    return retval;
}

void prplmesh_cleanup(void) {
    amxb_free(&app.bus_ctx);
    amxd_dm_clean(&app.dm);
    amxo_parser_clean(&app.parser);
    amxb_be_remove_all();
}