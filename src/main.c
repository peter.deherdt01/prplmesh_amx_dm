/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <signal.h>

#include "prplmesh.h"

#define DEFAULT_ODL_PATH "/etc/amx/"

static amxd_object_t* prepare_transaction(const char* path,
                                          amxd_trans_t* transaction) {
    amxd_object_t* object = amxd_dm_findf(prplmesh_get_dm(), "%s", path);
    if(!object) {
        return NULL;
    }

    amxd_status_t status = amxd_trans_init(transaction);
    if(status != amxd_status_ok) {
        return NULL;
    }

    status = amxd_trans_set_attr(transaction, amxd_tattr_change_ro, true);
    if(status != amxd_status_ok) {
        amxd_trans_clean(transaction);
        return NULL;
    }

    status = amxd_trans_select_object(transaction, object);
    if(status != amxd_status_ok) {
        amxd_trans_clean(transaction);
        return NULL;
    }

    return object;
}

static bool apply_transaction(amxd_trans_t* transaction) {
    bool ret = true;
    amxd_status_t status = amxd_trans_apply(transaction, prplmesh_get_dm());
    if(status != amxd_status_ok) {
        ret = false;
    }

    amxd_trans_clean(transaction);

    return ret;
}

static uint32_t add_instance(const char* path) {
    amxd_trans_t transaction;
    uint32_t index;

    amxd_object_t* object = prepare_transaction(path, &transaction);
    if(!object) {
        return 0;
    }

    amxd_trans_add_inst(&transaction, 0, NULL);

    if(apply_transaction(&transaction) != amxd_status_ok) {
        return 0;
    }

    index = amxd_object_get_index(transaction.current);

    return index;
}

static void prplmesh_start(AMXB_UNUSED const char* const sig_name,
                           AMXB_UNUSED const amxc_var_t* const data,
                           AMXB_UNUSED void* const priv) {
    add_instance("Controller.Network.Device");
    add_instance("Controller.Network.Device.1.Radio");
    add_instance("Controller.Network.Device.1.Radio");

    add_instance("Controller.Network.Device");
    add_instance("Controller.Network.Device.2.Radio");
    add_instance("Controller.Network.Device.2.Radio.1.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.1.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.1.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.1.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.1.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.1.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio");
    add_instance("Controller.Network.Device.2.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.2.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.2.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.2.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.2.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.2.CurrentOperatingClasses");
    add_instance("Controller.Network.Device.2.Radio.2.CurrentOperatingClasses");

    add_instance("Controller.Network.Device");
    add_instance("Controller.Network.Device.3.Radio");
    add_instance("Controller.Network.Device.3.Radio");
}

int main(int argc, char* argv[]) {
    int retval = 0;
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_dm_t* dm = NULL;
    char* odl = argv[2];

    // initialize bus agnostic connection and data model
    retval = prplmesh_init(argv[1], odl);
    if(retval != 0) {
        goto exit;
    }

    bus_ctx = prplmesh_get_bus_ctx();
    dm = prplmesh_get_dm();

    // create an eventloop (using libevent)
    if(prplmesh_el_create(bus_ctx) != 0) {
        retval = 5;
        goto exit;
    }

    printf("*************************************\n");
    printf("*         prplMesh started          *\n");
    printf("*************************************\n");

    amxp_slot_connect(&dm->sigmngr, "app:start", NULL, prplmesh_start, NULL);
    // trigger "app:start" signal
    //    - automatic data model instance counters use this signal to get initialized
    amxp_sigmngr_trigger_signal(&dm->sigmngr, "app:start", NULL);
    // and then start the eventloop
    if(prplmesh_el_start() != 0) {
        retval = 6;
    }
    // trigger "app:stop" signal
    amxp_sigmngr_trigger_signal(&dm->sigmngr, "app:stop", NULL);

    printf("*************************************\n");
    printf("*         prplMesh stopped          *\n");
    printf("*************************************\n");

exit:
    free(odl);
    // clean-up the eventloop (libevent)
    prplmesh_el_destroy();
    // cleanup bus agnostic connection and data model
    prplmesh_cleanup();
    return retval;
}
