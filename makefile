include makefile.inc

# compilation and linking flags
CFLAGS += -Werror -Wall -Wextra \
          -Wformat=2 -Wno-unused-parameter -Wshadow \
          -Wwrite-strings -Wredundant-decls -Wmissing-include-dirs \
		  -Wno-format-nonliteral  -Wno-format-nonliteral \
          -g3 -Wmissing-declarations $(addprefix -I ,$(INCDIRS)) -pthread 
		  
ifeq ($(CC_NAME),g++)
    CFLAGS += -std=c++2a
else
	CFLAGS += -Wstrict-prototypes -Wold-style-definition -Wnested-externs -std=c11
endif

LDFLAGS += -levent -lamxc -lamxp -lamxd -lamxb -lamxo

# helper functions - used in multiple targets
define install_to
	$(INSTALL) -d $(1)$(PREFIX)$(INSTALL_BIN_DIR)/
	$(INSTALL) -m 0755 $(TARGET) $(1)$(PREFIX)$(INSTALL_BIN_DIR)/$(TARGET_NAME)
	$(INSTALL) -d $(1)$(INSTALL_CFG_DIR)/$(TARGET_NAME)
	$(foreach def,$(ODLS),$(INSTALL) -m 0644 $(def) $(1)$(INSTALL_CFG_DIR)/$(TARGET_NAME)/;)
endef

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak 
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md 
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md  
	$(ECHO) "" >> CHANGELOG.md 
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md 
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	$(MKDIR) -p $@

changelog:
	$(call create_changelog) 
	
install: $(TARGET)
	$(call install_to,$(DEST)) 

test: 
	make -C tests
	make -C tests coverage

clean:
	rm -rf ./output/ $(TARGET)
	find . -name "run_test" -delete

.PHONY: clean
