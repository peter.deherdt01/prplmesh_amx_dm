
# prplMesg - Data Model

[[_TOC_]]

## Building

Change your working directory to the root of this repo and use `make`.

```Bash
cd <prplmesh>/
make
```

After the `make` has finished, you can install it by calling `make install`.
The `make install` will install in your build output directory and not in your system directories.
You can provide the `DEST` variable to change the root directory to where it must be installed.

Examples:

Default install to output directory
```bash
$ make install
/usr/bin/install -d ./output/x86_64-linux-gnu/build/usr/bin/
/usr/bin/install -m 0755 ./output/x86_64-linux-gnu/object/prplmesh ./output/x86_64-linux-gnu/build/usr/bin/prplmesh
/usr/bin/install -d ./output/x86_64-linux-gnu/build/etc/amx/prplmesh
/usr/bin/install -m 0644 ./odl/dissasociaton_event_data.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/sta.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/dm_defaults.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/radio.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/association_event_data.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/device.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/channel_scan.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/capabilities.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/scan_result.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/disassociation_event.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/controller.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/neighbor_bss.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/association_event.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/unassociated_sta.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/bss.odl ./output/x86_64-linux-gnu/build/etc/amx/prplmesh/; 
```

Install in your system root

```bash
$ sudo make DEST="" install
/usr/bin/install -d /usr/bin/
/usr/bin/install -m 0755 ./output/x86_64-linux-gnu/object/prplmesh /usr/bin/prplmesh
/usr/bin/install -d /etc/amx/prplmesh
/usr/bin/install -m 0644 ./odl/dissasociaton_event_data.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/sta.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/dm_defaults.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/radio.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/association_event_data.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/device.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/channel_scan.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/capabilities.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/scan_result.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/disassociation_event.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/controller.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/neighbor_bss.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/association_event.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/unassociated_sta.odl /etc/amx/prplmesh/; /usr/bin/install -m 0644 ./odl/bss.odl /etc/amx/prplmesh/; 
```

## Running `prplMesh` data model

### Run Prerequisites

To be able to run this example you will need to:

- Build and install the example
- Install an `Ambiorix` bus back-end

A software bus must be running and the correct back-end must be installed.

It is assumed that you have `ubus` running and the `ubus` tool is installed (so the `amxb-ubus.so` should be available as well).

An environment variable needs to be defined as well and must point to the bus back-end that needs to be loaded.

```bash
export AMXB_BACKEND=/usr/bin/mods/amxb/mod-amxb-ubus.so
```

### Running

Once it is built and everything is installed you can launch the example.

  ```bash
  $ prplmesh ubus:/var/run/ubus.sock /etc/amx/prplmesh/controller.odl
  ```

The application needs two arguments at the command line, an URI it has to connect to and the `main` odl it needs to load. 

The URI is pointing to the bus socket, in this case it is the `ubus` unix domain socket, which is by default `/var/run/ubus.sock`

The `main` odl contains the data model definition.

When the application is started, it will print the configuration options, followed by this message.

```text
*************************************
*         prplMesh started          *
*************************************
```

That indicates that all went well and the `prplMesh` data model is available.

### Accessing Data Model

- Dump list of objects

```
$ ubus list
Controller
Controller.Network
Controller.Network.Device
Controller.Network.Device.1
Controller.Network.Device.1.MultiAPCapabilities
Controller.Network.Device.1.Radio
Controller.Network.Device.1.Radio.1
Controller.Network.Device.1.Radio.1.BSS
Controller.Network.Device.1.Radio.1.BackhaulSTA
Controller.Network.Device.1.Radio.1.Capabilities
Controller.Network.Device.1.Radio.1.Capabilities.OperatingClasses
Controller.Network.Device.1.Radio.1.CurrentOperatingClasses
Controller.Network.Device.1.Radio.1.ScanResult
Controller.Network.Device.1.Radio.1.ScanResult.OpClassScan
Controller.Network.Device.1.Radio.1.UnassociatedSTA
Controller.Network.Device.1.Radio.2
Controller.Network.Device.1.Radio.2.BSS
Controller.Network.Device.1.Radio.2.BackhaulSTA
Controller.Network.Device.1.Radio.2.Capabilities
Controller.Network.Device.1.Radio.2.Capabilities.OperatingClasses
Controller.Network.Device.1.Radio.2.CurrentOperatingClasses
Controller.Network.Device.1.Radio.2.ScanResult
Controller.Network.Device.1.Radio.2.ScanResult.OpClassScan
Controller.Network.Device.1.Radio.2.UnassociatedSTA
Controller.Network.Device.1.Radio.BackhaulSTA
Controller.Network.Device.1.Radio.Capabilities
Controller.Network.Device.1.Radio.Capabilities.OperatingClasses
Controller.Network.Device.1.Radio.ScanResult
Controller.Network.Device.1.Radio.ScanResult.OpClassScan
Controller.Network.Device.1.UnassociatedSTA
Controller.Network.Device.2
Controller.Network.Device.2.MultiAPCapabilities
Controller.Network.Device.2.Radio
Controller.Network.Device.2.Radio.1
Controller.Network.Device.2.Radio.1.BSS
Controller.Network.Device.2.Radio.1.BackhaulSTA
Controller.Network.Device.2.Radio.1.Capabilities
Controller.Network.Device.2.Radio.1.Capabilities.OperatingClasses
Controller.Network.Device.2.Radio.1.CurrentOperatingClasses
Controller.Network.Device.2.Radio.1.CurrentOperatingClasses.1
Controller.Network.Device.2.Radio.1.CurrentOperatingClasses.2
Controller.Network.Device.2.Radio.1.CurrentOperatingClasses.3
Controller.Network.Device.2.Radio.1.CurrentOperatingClasses.4
Controller.Network.Device.2.Radio.1.CurrentOperatingClasses.5
Controller.Network.Device.2.Radio.1.CurrentOperatingClasses.6
Controller.Network.Device.2.Radio.1.ScanResult
Controller.Network.Device.2.Radio.1.ScanResult.OpClassScan
Controller.Network.Device.2.Radio.1.UnassociatedSTA
Controller.Network.Device.2.Radio.2
Controller.Network.Device.2.Radio.2.BSS
Controller.Network.Device.2.Radio.2.BackhaulSTA
Controller.Network.Device.2.Radio.2.Capabilities
Controller.Network.Device.2.Radio.2.Capabilities.OperatingClasses
Controller.Network.Device.2.Radio.2.CurrentOperatingClasses
Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.1
Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.2
Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.3
Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.4
Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.5
Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.6
Controller.Network.Device.2.Radio.2.ScanResult
Controller.Network.Device.2.Radio.2.ScanResult.OpClassScan
Controller.Network.Device.2.Radio.2.UnassociatedSTA
Controller.Network.Device.2.Radio.BackhaulSTA
Controller.Network.Device.2.Radio.Capabilities
Controller.Network.Device.2.Radio.Capabilities.OperatingClasses
Controller.Network.Device.2.Radio.ScanResult
Controller.Network.Device.2.Radio.ScanResult.OpClassScan
Controller.Network.Device.2.UnassociatedSTA
Controller.Network.Device.3
Controller.Network.Device.3.MultiAPCapabilities
Controller.Network.Device.3.Radio
Controller.Network.Device.3.Radio.1
Controller.Network.Device.3.Radio.1.BSS
Controller.Network.Device.3.Radio.1.BackhaulSTA
Controller.Network.Device.3.Radio.1.Capabilities
Controller.Network.Device.3.Radio.1.Capabilities.OperatingClasses
Controller.Network.Device.3.Radio.1.CurrentOperatingClasses
Controller.Network.Device.3.Radio.1.ScanResult
Controller.Network.Device.3.Radio.1.ScanResult.OpClassScan
Controller.Network.Device.3.Radio.1.UnassociatedSTA
Controller.Network.Device.3.Radio.2
Controller.Network.Device.3.Radio.2.BSS
Controller.Network.Device.3.Radio.2.BackhaulSTA
Controller.Network.Device.3.Radio.2.Capabilities
Controller.Network.Device.3.Radio.2.Capabilities.OperatingClasses
Controller.Network.Device.3.Radio.2.CurrentOperatingClasses
Controller.Network.Device.3.Radio.2.ScanResult
Controller.Network.Device.3.Radio.2.ScanResult.OpClassScan
Controller.Network.Device.3.Radio.2.UnassociatedSTA
Controller.Network.Device.3.Radio.BackhaulSTA
Controller.Network.Device.3.Radio.Capabilities
Controller.Network.Device.3.Radio.Capabilities.OperatingClasses
Controller.Network.Device.3.Radio.ScanResult
Controller.Network.Device.3.Radio.ScanResult.OpClassScan
Controller.Network.Device.3.UnassociatedSTA
Notification
Notification.AssociationEvent
Notification.DisassociationEvent
```

- Dump content of object

```
$ ubus call Controller.Network.Device.2.Radio.2 get
{
        "Controller.Network.Device.2.Radio.2.": {
                "Enabled": false,
                "ReceiveOther": 0,
                "NumberOfUnassocSTA": 0,
                "NumberOfCurrOpClass": 6,
                "NumberOfBSS": 0,
                "Noise": 0,
                "ID": "0",
                "Transmit": 0,
                "Utilization": 0,
                "ReceiveSelf": 0
        }
}
```

- Introspect an object

```
$ ubus call Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.1 describe
{
        "attributes": {
                "private": false,
                "read-only": false,
                "locked": false,
                "protected": false,
                "persistent": false
        },
        "object": "Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.1.",
        "index": 1,
        "name": "1",
        "type_name": "instance",
        "type_id": 3,
        "path": "Controller.Network.Device.2.Radio.2.CurrentOperatingClasses.1."
}
```

